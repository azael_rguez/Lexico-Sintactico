# Lexical-Syntactic

Project to understand how an compiler works.
Using Python 3.6 to recognize Python syntax.

## Getting Started

### Main dependencies

* Ply: 3.11
* Colorama: 0.4.1

### Installing dependencies

```
pip install ply colorama
```

### Running the tests

```
python main.py
```

## Authors

* Azael Rodríguez - (Development Lead) [[Gitlab](https://gitlab.com/azael_rguez), [Twitter](https://twitter.com/azael_rguez)]

## Licence

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
