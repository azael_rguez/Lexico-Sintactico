#!/usr/bin/env python
# -*- coding: utf-8 -*-

from analizer.lexical import *
from analizer.syntactic import *
from colorama import init, Fore

if __name__ == "__main__":
    try:
        init()
        input_file = open("tests/input.py", "r")
        data = str(input_file.read())

        # Lexical
        print(Fore.WHITE)
        result = test(data)
        string = ""
        for lex in result:
            string += lex + "\n"
        print(string)
        
        # Syntactic & Semantic
        print(Fore.WHITE)
        result = syntactic_test(data)
        string = ""
        for item in result:
            string += item + "\n"
        print(string)
    except IOError as e:
        print(e)
