#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ply.yacc as yacc
from analizer.lexical import tokens
from analizer.lexical import analizer
from colorama import init, Fore

global result
result = []
names = {}
position = 0
init()

def p_statement_assign(t):
    'statement : variable assignation expression'
    names[t[1]] = t[3]
    pass

def p_statement_expr(t):
    'statement : expression'
    t[0] = t[1]
    pass

def p_expression_variable(t):
    'expression : variable'
    try:
        t[0] = names[t[1]]
    except LookupError:
        result.append(Fore.RED + ">>> Error of undefined variable '" + t[1] + "' at line " + str(position) + Fore.WHITE)
        t[0] = None
    pass

def p_expression_group(t):
    '''expression   : roundLeft expression roundRight
                    | squareLeft expression squareRight
                    | curlyLeft expression curlyRight'''
    t[0] = t[2]
    pass

def p_expression_oper(t):
    '''expression : expression addition expression
                  | expression substration expression
                  | expression multiplication expression
                  | expression divition expression
                  | expression module expression
                  | expression exponential expression'''
    try:
        if t[2] == '+'      : 
            if ((type(t[1]) is int or type(t[1]) is float) 
                    and (type(t[3]) is int or type(t[3]) is float)):
                t[0] = t[1] + t[3]
            else: 
                t[0] = str(t[1]) + str(t[3])
        elif t[2] == '-'    : t[0] = t[1] - t[3]
        elif t[2] == '*'    : t[0] = t[1] * t[3]
        elif t[2] == '/'    : t[0] = t[1] / t[3]
        elif t[2] == '%'    : t[0] = t[1] % t[3]
        elif t[2] == '**'   : t[0] = t[1] ** t[3]
    except:
        t[0] = None
        result.append(Fore.RED + '>>> Error of unsupported operand \'' + t[2] + '\' at line ' + str(position) + ' for strings operations' + Fore.WHITE)
    pass

def p_expression_bool(t):
    '''expression   :   expression andSimbol expression
                    |   expression orSimbol expression
                    |   expression negation expression
                    |   roundLeft expression andSimbol expression roundRight
                    |   roundLeft expression orSimbol expression roundRight
                    |   roundLeft expression negation expression roundRight'''
    if t[2] == '&&'     : t[0] = t[1] and t[3]
    elif t[2] == '||'   : t[0] = t[1] or t[3]
    elif t[2] == '!'    : t[0] =  t[1] is not t[3]
    elif t[3] == '&&'   : t[0] = t[2] and t[4]
    elif t[3] == '||'   : t[0] = t[2] or t[4]
    elif t[3] == '!'    : t[0] =  t[2] is not t[4]
    pass

def p_expression_logic(t):
    '''expression   :   expression lessThan expression
                    |   expression greaterThan expression
                    |   expression lessEqual expression
                    |   expression greaterEqual expression
                    |   expression equal expression
                    |   expression different expression
                    |   roundLeft expression roundRight lessThan roundLeft expression roundRight
                    |   roundLeft expression roundRight greaterThan roundLeft expression roundRight
                    |   roundLeft expression roundRight lessEqual roundLeft expression roundRight
                    |   roundLeft expression roundRight greaterEqual roundLeft expression roundRight
                    |   roundLeft expression roundRight equal roundLeft expression roundRight
                    |   roundLeft expression roundRight different roundLeft expression roundRight'''
    if t[2] == '<'      : t[0] = t[1] < t[3]
    elif t[2] == '>'    : t[0] = t[1] > t[3]
    elif t[2] == '<='   : t[0] = t[1] <= t[3]
    elif t[2] == '>='   : t[0] = t[1] >= t[3]
    elif t[2] == '=='   : t[0] = t[1] is t[3]
    elif t[2] == '!='   : t[0] = t[1] is not t[3]
    elif t[3] == '<'    : t[0] = t[2] < t[4]
    elif t[2] == '>'    : t[0] = t[2] > t[4]
    elif t[3] == '<='   : t[0] = t[2] <= t[4]
    elif t[3] == '>='   : t[0] = t[2] >= t[4]
    elif t[3] == '=='   : t[0] = t[2] is t[4]
    elif t[3] == '!='   : t[0] = t[2] is not t[4]
    pass

def p_expression_string(t):
    'expression : string'
    t[0] = t[1]
    pass

def p_expression_char(t):
    'expression : char'
    t[0] = t[1]
    pass

def p_expression_float(t):
    'expression : float'
    t[0] = t[1]
    pass

def p_expression_constants(t):
    'expression : constants'
    t[0] = t[1]
    pass

def p_expression_integer(t):
    'expression : integer'
    t[0] = t[1]
    pass

def p_expression_print(t):
    'expression : print roundLeft expression roundRight'
    t[0] = t[3]
    pass

def evaluateBrackets(astr, brackets):
    iparens = brackets
    parens = dict(zip(iparens, iparens))
    closing = parens.values()
    stack = []
    for c in astr:
        d = parens.get(c, None)
        if d:
            stack.append(d)
        elif c in closing:
            if not stack or c != stack.pop():
                return False
    return not stack

def p_error(t):
    if t:
        if (str(t.type) != 'roundRight' and str(t.type) != 'roundLeft' and 
            str(t.type) != 'squareRight' and str(t.type) != 'squareLeft' and
            str(t.type) != 'curlyRight' and str(t.type) != 'curlyLeft'):
            result.append(Fore.RED + '>>> Error of ' + str(t.type) + ' at line ' + str(position) + ' before \'' + str(t.value) + '\'' + Fore.WHITE)
    pass

parser = yacc.yacc()

def syntactic_test(data):
    global position
    result.clear()
    for item in data.splitlines():
        if item:
            position += 1
            gram = parser.parse(item)
            if ('(' in item or ')' in item):
                if evaluateBrackets(str(item), iter('()')) is False:
                    result.append(Fore.RED + '>>> Error of balanced round brackets at line ' + str(position) + 
                    ' in \'' + str(item).strip() + '\'' + Fore.WHITE)
            if ('[' in item or ']' in item):
                if evaluateBrackets(str(item), iter('[]')) is False:
                    result.append(Fore.RED + '>>> Error of balanced square brackets at line ' + str(position) + 
                    ' in \'' + str(item).strip() + '\'' + Fore.WHITE)
            if ('{' in item or '}' in item):
                if evaluateBrackets(str(item), iter('[]')) is False:
                    result.append(Fore.RED + '>>> Error of balanced curly brackets at line ' + str(position) + 
                    ' in \'' + str(item).strip() + '\'' + Fore.WHITE)
            
            if gram is not None:
                if 'print' in item:
                    gram = str(gram)
                    if item.find('\"'): gram = gram.replace('\"', '')
                    if item.find('\''): gram = gram.replace('\'', '')
                    result.append(gram)
    return result
