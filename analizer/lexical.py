#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ply.lex as lex
from colorama import init, Fore

init()

global lex_result
lex_result = []

reserved_word = (
    'and',
    'or',
    'not',
    'if',
    'elif',
    'else',
    'for',
    'while',
    'in',
)

tokens = reserved_word + (
    #functions
    'print',
    'len',
    'sqrt',
    'range',

    # variable
    'variable',

    # Data types
    'string',
    'char',
    'float',
    'integer',

    # Constants: True, False, None
    'constants',

    # Math operators
    'assignation',
    'addition',
    'substration',
    'multiplication',
    'divition',
    'exponential',
    'module',

    # Logical symbols: && || ! < <= > >= == !=
    'andSimbol',
    'orSimbol',
    'negation',
    'lessThan',
    'lessEqual',
    'greaterThan',
    'greaterEqual',
    'equal',
    'different',

    # Brackets: ()[]{}
    'roundLeft',
    'roundRight',
    'squareLeft',
    'squareRight',
    'curlyLeft',
    'curlyRight',

    # quotes, dot, and coma
    'simpleQuote',
    'doubleQuote',
    'dot',
    'coma',
    'colon',
    'semicolon',
)

t_addition = r'\+'
t_substration = r'-'
t_dot = r'\.'
t_multiplication = r'\*'
t_divition = r'/'
t_module = r'\%'
t_exponential = r'(\*{2} | \^)'
t_assignation = r'='

t_andSimbol = r'\&\&'
t_orSimbol = r'\|\|'
t_different = r'!='
t_negation = r'\!'
t_lessThan = r'<'
t_lessEqual = r'<='
t_greaterThan = r'>'
t_greaterEqual = r'>='
t_equal = r'=='

t_coma = r','
t_colon = r':'
t_semicolon = r';'
t_roundLeft = r'\('
t_roundRight = r'\)'
t_squareLeft = r'\['
t_squareRight = r'\]'
t_curlyLeft = r'{'
t_curlyRight = r'}'
t_simpleQuote = r'\''
t_doubleQuote = r'\"'

def t_and(t):
    r'and'
    return t

def t_or(t):
    r'or'
    return t

def t_not(t):
    r'not'
    return t

def t_if(t):
    r'if'
    return t

def t_elif(t):
    r'elif'
    return t

def t_else(t):
    r'else'
    return t

def t_for(t):
    r'for'
    return t

def t_while(t):
    r'while'
    return t

def t_in(t):
    r'in'
    return t

def t_range(t):
    r'range'
    return t

def t_float(t):
    r'\d+\.\d+'
    t.value = float(t.value)
    return t

def t_integer(t):
    r'\d+'
    t.value = int(t.value)
    return t

def t_string(t):
    r'\".*\"'
    return t

def t_char(t):
    r'\'.*\''
    return t

def t_constants(t):
    r'(True|False|None)'
    return t

def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def t_print(t):
    r'print'
    return t

def t_len(t):
    r'len'
    return t

def t_sqrt(t):
    r'sqrt'
    return t

def t_variable(t):
    r'\w+(_\d\w)*'
    return t

# Ignore comments
def t_comment(t):
    r'\#.*'
    pass

# Ignore spaces
t_ignore = ' \t'

def t_error(t):
    state = Fore.RED + "{0:<30}Type: {1:<15}Line: {2}".format(t.value[0], 'Invalid token', t.lineno) + Fore.WHITE
    lex_result.append(state)
    t.lexer.skip(1)
    pass

def test(data):
    analizer.input(data)
    lex_result.clear()
    while True:
        tok = analizer.token()
        if not tok:
            break
        state = "{0:<30}Type: {1:<15}Line: {2}".format(tok.value, tok.type, tok.lineno)
        lex_result.append(state)
    return lex_result

analizer = lex.lex()
